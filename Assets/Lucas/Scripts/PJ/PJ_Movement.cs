using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PJ_Movement : MonoBehaviour
{
    #region Variables
    //INPUT SYSTEM
    [SerializeField]
    private InputActionAsset m_Input;
    private InputAction m_movement;


    //RIGIDBODY
    private Rigidbody m_Rigidbody;

    //MOVEMENT
    [SerializeField]
    private float m_Speed;
    private Vector3 m_Movement;

    //STATES
    [SerializeField]
    private SwitchMachineStates m_CurrentState;

    //ROTATION
    private bool isRotating = false;
    private float rotationSpeed = 3f;

    #endregion

    #region States
    private enum SwitchMachineStates
    {
        NONE, IDLE, WALK,
    };


    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;

        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Rigidbody.velocity = new Vector3(0, m_Rigidbody.velocity.y, 0);
                break;


            case SwitchMachineStates.WALK:



                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;



            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:
                m_Rigidbody.velocity = new Vector3(m_Movement.x, m_Rigidbody.velocity.y, m_Movement.y);
                transform.forward = new Vector3(m_Movement.x, 0, m_Movement.y);
                break;

            default:
                break;
        }
    }

    #endregion

    #region Init
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

    }

    private void Start()
    {
        m_Input.FindActionMap("Movement").FindAction("Move").performed += OnMove;
        m_Input.FindActionMap("Movement").FindAction("Move").canceled += OnCancelMove;
        m_movement = m_Input.FindActionMap("Movement").FindAction("Move");
        m_Input.FindActionMap("Movement").Enable();
    }

    #endregion

    #region Update

    private void Update()
    {
        UpdateState();
    }
    #endregion

    #region Input_Functions
    public void OnMove(InputAction.CallbackContext context)
    {
        m_Movement = m_movement.ReadValue<Vector2>() * m_Speed;
        if (m_Movement.x < 0)
        {
            StartCoroutine(RotatePlayer(-90f));
        }
        else if (m_Movement.x > 0)
        {
            StartCoroutine(RotatePlayer(90f));
        }

        ChangeState(SwitchMachineStates.WALK);
    }

    public void OnCancelMove(InputAction.CallbackContext context)
    {
        ChangeState(SwitchMachineStates.IDLE);
    }
    #endregion

    #region Functions_Movement
    private IEnumerator RotatePlayer(float targetAngle)
    {
        isRotating = true;
        float totalRotation = 0f;
        m_Movement = m_movement.ReadValue<Vector2>() * m_Speed;
        while ((m_Movement.x < 0 || m_Movement.x > 0) && m_CurrentState == SwitchMachineStates.WALK)
        {

            if (m_Movement.x < 0)
            {
                float rotationAmount = -rotationSpeed * Time.deltaTime;
                transform.Rotate(Vector3.up * rotationAmount);
                totalRotation += Mathf.Abs(rotationAmount);
            }
            else
            {
                float rotationAmount = rotationSpeed * Time.deltaTime;
                transform.Rotate(Vector3.up * rotationAmount);
                totalRotation += Mathf.Abs(rotationAmount);
            }


            yield return null;
        }

        isRotating = false;
    }

    #endregion

}
